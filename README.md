# quickcheats [![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

This is a datapack that allows you to set up a creative "test world" in your
survival world or on a survival server. It's worse than any server plugin could
ever be, but it's an experiment with Minecraft's new custom dimensions.

## Usage

- You're in the survival world, you want to teleport to the creative one, you
  type `/trigger creative`
- You're in the creative world, you want to teleport back to the survival one,
  you type `/trigger survival`

Yep, it's really straight forward!

## Attack vectors

The following ways to exploit the system are prevented:

- [x] Transferring items. The inventory is cleared when you go survival, and you
      can't go creative without first stashing all your items in a chest, or it
      will complain.
- [x] Transferring XP. The XP is backed up to a scoreboard before you go
      creative, and will be given back to you after you go survival.
- [x] Going creative twice, to overwrite the XP scoreboard. There's a check to
      make sure you can only trigger the respective mode if you're not already
      in it.
- [x] Nether portals. You can't make 'em in this dimension. I suppose it's a
      Minecraft feature or something.
- [x] End portals. Same as above.
- [x] Killing yourself with `keepInventory`. This datapack will make sure to
      clear your inv if you die in the creative world.
- [ ] **Avoid the scary night in creative mode.** Time still progresses in both
      worlds (I think, at least!), so this is a potential "exploit". Never the
      less though, the player *could* just sleep in a bed or hide underground
      anyway.

## Datapack download

This datapack is being generated from some Nix code [using this
builder](https://gitlab.com/jD91mZM2/nur-packages/-/commit/2614f9f5650c202fa8abc63592da91c8dd7d0c52).
It avoids pesky repetition, you're not forced to follow a structure, and it's
just so much better than JSON.

You can download the generated archive from each commit. There's a CI job
running on each update and builds the zip archive for you. You can access it
like this:

1. ![Open CI](https://i.imgur.com/5hfFlrE.png)
1. ![Open stage "build"](https://i.imgur.com/AqwTpxC.png)
1. ![Browse job artifacts](https://i.imgur.com/qjwuScW.png)
1. ![Download quickcheats.zip](https://i.imgur.com/MsMO3db.png)
