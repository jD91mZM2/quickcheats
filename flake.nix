{
  description = "A minecraft datapack that adds a creative-mode dimension while enforcing that players don't bring items";

  inputs.nur = {
    # type = "path";
    # path = "/home/user/dotfiles/nur-packages";
    type = "git";
    url = "https://gitlab.com/jD91mZM2/nur-packages.git";
  };

  outputs = { self, nixpkgs, nur }:
    let
      forAllSystems = nixpkgs.lib.genAttrs ["x86_64-linux"];
    in {
      packages = forAllSystems (system: rec {
        datapack = nixpkgs.legacyPackages."${system}".callPackage ./datapack.nix {
          inherit nur;
        };
        install = nur.builders."${system}".minecraft.installDatapack datapack "quickcheats";
      });
      defaultPackage = forAllSystems (system: self.packages."${system}".datapack);
    };
}
