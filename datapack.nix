{ pkgs, nur, system, ... }:

with nur.builders."${system}".minecraft;

mkDatapack {
  meta = {
    name = "quickcheats";
    description = "Adds a creative-mode dimension while enforcing that players don't bring items";
  };
  additions = {
    dimension_type = {
      type = dimensionSettings.overworld;
    };
    dimension = {
      creative_world = {
        type = "quickcheats:type";
        generator = genNormal {
          seed = 0;
        };
      };
    };

    functions = {
      tick = builtins.readFile ./functions/tick.mcfunction;
      init = builtins.readFile ./functions/init.mcfunction;
    };
  };
  modifications.tags.functions = {
    tick = ["quickcheats:tick"];
    load = ["quickcheats:init"];
  };
}
