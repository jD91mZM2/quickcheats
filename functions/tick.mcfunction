# Enable changing scores for everyone using /trigger
scoreboard players enable @a creative
scoreboard players enable @a survival

# Tag all people who should be creative mode
tag @a remove qc_in_creative
tag @a[nbt={Dimension:"quickcheats:creative_world"}] add qc_in_creative

# Fix gamemodes accordingly
gamemode creative @a[gamemode=survival,tag=qc_in_creative]
gamemode survival @a[gamemode=creative,tag=!qc_in_creative]

# Don't accidentally sleep in bed, and trap yourself in creative
execute at @a[tag=qc_in_creative] run fill ~-5 ~-5 ~-5 ~5 ~5 ~5 minecraft:air replace #minecraft:beds

#
# Allow teleporting to creative world
#

# Filter out people trying to transfer items
tellraw @a[scores={creative=1..},nbt=!{Inventory:[]}] {"text":"Your inventory is not empty. You would lose all your items.","color":"red"}
scoreboard players reset @a[scores={creative=1..},nbt=!{Inventory:[]}] creative

# Filter out creative players
tellraw @a[scores={creative=1..},tag=qc_in_creative] {"text":"You're already creative","color":"red"}
scoreboard players reset @a[scores={creative=1..},tag=qc_in_creative] creative

# Save XP, both levels and points, and then clear (no, they cannot be cleared individually, they're related somehow)
execute as @a[scores={creative=1..}] store result score @s qc_xp_levels run xp query @s levels
execute as @a[scores={creative=1..}] store result score @s qc_xp_points run xp query @s points
xp set @a[scores={creative=1..}] 0 levels
xp set @a[scores={creative=1..}] 0 points

# Teleport
execute as @a[scores={creative=1..}] at @s in quickcheats:creative_world run tp @s 0 255 0
# NOTE: For some reason, just doing this causes the player to go underground:
# spreadplayers 0 0 5 10 false @a[scores={creative=1..}]
execute as @a[scores={creative=1..}] at @s run spreadplayers 0 0 5 10 false @s
scoreboard players reset @a[scores={creative=1..}] creative

# Death means go back to survival
scoreboard players set @a[scores={qc_death=1..},tag=qc_in_creative] survival 1
scoreboard players reset @a[scores={qc_death=1..}] qc_death

#
# Allow teleporting back to survival world
#

# Filter out survival players
tellraw @a[scores={survival=1..},tag=!qc_in_creative] {"text":"You're already survival","color":"red"}
scoreboard players reset @a[scores={survival=1..},tag=!qc_in_creative] survival

# Teleport to survival world, just so qc_in_creative check doesn't apply later
execute as @a[scores={survival=1..}] in minecraft:overworld run tp @s 0 255 0

# Clear inventory (in case of keepInventory) and kill player
clear @a[scores={survival=1..}]
kill @a[scores={survival=1..}]

# Clear xp
xp set @a[scores={survival=1..}] 0 levels
xp set @a[scores={survival=1..}] 0 points

# Mark as needing xp payment
tag @a[scores={survival=1..}] add qc_xp_debt

# Reset score
scoreboard players reset @a[scores={survival=1..}] survival

# Pay some levels of xp debt each tick
xp add @a[tag=qc_xp_debt,scores={qc_xp_levels=100..}] 100 levels
scoreboard players remove @a[tag=qc_xp_debt,scores={qc_xp_levels=100..}] qc_xp_levels 100
xp add @a[tag=qc_xp_debt,scores={qc_xp_levels=10..}] 10 levels
scoreboard players remove @a[tag=qc_xp_debt,scores={qc_xp_levels=10..}] qc_xp_levels 10
xp add @a[tag=qc_xp_debt,scores={qc_xp_levels=1..}] 1 levels
scoreboard players remove @a[tag=qc_xp_debt,scores={qc_xp_levels=1..}] qc_xp_levels 1

# Pay some points of xp debt each tick (after levels)
execute as @a[tag=qc_xp_debt,scores={qc_xp_levels=0,qc_xp_points=10..}] run xp add @s 10 points
scoreboard players remove @a[tag=qc_xp_debt,scores={qc_xp_levels=0,qc_xp_points=10..}] qc_xp_points 10
execute as @a[tag=qc_xp_debt,scores={qc_xp_levels=0,qc_xp_points=1..}] run xp add @s 1 points
scoreboard players remove @a[tag=qc_xp_debt,scores={qc_xp_levels=0,qc_xp_points=1..}] qc_xp_points 1

# Remove xp debt tag from players with paid debt
tag @a[tag=qc_xp_debt,scores={qc_xp_levels=0,qc_xp_points=0}] remove qc_xp_debt
