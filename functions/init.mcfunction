scoreboard objectives add creative trigger "Teleport to creative world"
scoreboard objectives add survival trigger "Teleport back to normal overworld"

scoreboard objectives add qc_xp_levels dummy "Saved XP levels"
scoreboard objectives add qc_xp_points dummy "Saved XP points"

scoreboard objectives add qc_death deathCount "Clear inventory on death"

tellraw @a "Quickcheats loaded. Creative world can be reached by /trigger creative"
